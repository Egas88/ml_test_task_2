from flask import Flask, request, render_template


app = Flask(__name__)


@app.route('/')
def main_page():
    return render_template('index.html')


@app.route('/post_form', methods=['POST'])
def process_form():
    text = request.form['text']
    errors = [["Ашибка", "Орфографическая", "слово должно писаться как 'Ошибка' "]]
    return render_template('index.html', text=text, err=errors)


@app.route('/post_json', methods=['POST'])
def process_json():
    content_type = request.headers.get('Content-Type')
    if content_type == 'application/json':
        json = request.get_json()
        return json
    else:
        return 'Content-Type not supported!'


if __name__ == '__main__':
    app.run(debug=True)
